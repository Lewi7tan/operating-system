/*
As RUST does not know, that we're writing to VGA buffer (may think it's just RAM),
it can decide to optimize the program and delete writes to 0xb8000, because the
address is never accessed and it doesn't know that we're writing to the screen.

Therefore Volatile is used. It provides write and read methods.
*/
use volatile::Volatile;
use spin::Mutex;
use lazy_static::lazy_static;
use core::fmt;

//TODO: implement ANSI escape codes

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::drivers::vga_buf::_print(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}

#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
    use core::fmt::Write;
    use x86_64::instructions::interrupts;

    interrupts::without_interrupts(|| {
        WRITER.lock().write_fmt(args).unwrap();
        WRITER.lock().update_cursor();
    });
}

lazy_static! {
    pub static ref WRITER: Mutex<ScreenWriter> = Mutex::new(ScreenWriter {
        current_row_position: 0,
        current_column_position: 0,
        current_colour_code: ColourCode::new(Colour::White, Colour::Black),
        char_buffer: unsafe { &mut *(0xb8000 as *mut CharBuffer) },
    });
}


#[allow(dead_code)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)] //every value inside enum is represented as u8 value
pub enum Colour {
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGray = 7,
    DarkGray = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    Pink = 13,
    Yellow = 14,
    White = 15,
}


#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(transparent)]
struct ColourCode(u8);

impl ColourCode {
    fn new(foreground: Colour, background: Colour) -> ColourCode {
        ColourCode((background as u8) << 4 | (foreground as u8))
    }
}

/*
 VGA Text Buffer character representation:
 Bits:      Value:
 0-7        ASCII code
 8-11       foreground colour
 12-15      background colour
 */
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(C)]
struct SingleChar {
    ascii_code: u8,
    colour_code: ColourCode,
}

const BUFFER_HEIGHT: usize = 25;
const BUFFER_WIDTH: usize = 80;

#[repr(transparent)]
struct CharBuffer {
    chars: [[Volatile<SingleChar>; BUFFER_WIDTH]; BUFFER_HEIGHT],
}


pub struct ScreenWriter {
    current_row_position: usize,
    current_column_position: usize,
    current_colour_code: ColourCode,
    char_buffer: &'static mut CharBuffer,
}

impl ScreenWriter {
    pub fn write_string(&mut self, s: &str) {
        for byte in s.bytes() {
            match byte {
                // printable ASCII byte or newline
                0x20..=0x7e | b'\n' => self.write_byte(byte),
                // not part of printable ASCII range
                _ => self.write_byte(0xfe),
            }
        }
    }

    pub fn write_byte(&mut self, byte: u8) {
        match byte {
            b'\n' => self.new_line(),
            byte => self.write_with_line_overflow_if_needed(byte),
        }
    }

    fn write_with_line_overflow_if_needed(&mut self, ascii_code: u8) {
        self.pass_character_to_text_buffer(ascii_code);

        self.current_column_position += 1;

        if self.current_column_position == BUFFER_WIDTH {
            self.new_line();
        }
    }

    fn pass_character_to_text_buffer(&mut self, ascii_code: u8) {
        let row = self.current_row_position;
        let col = self.current_column_position;
        let colour_code = self.current_colour_code;

        self.char_buffer.chars[row][col].write(SingleChar {
            ascii_code,
            colour_code,
        });
    }

    fn new_line(&mut self) {
        if self.current_row_position == (BUFFER_HEIGHT - 1) {
            self.scroll();
        } else {
            self.current_row_position += 1;
        }

        self.current_column_position = 0;
    }

    fn scroll(&mut self) {
        for row in 1..BUFFER_HEIGHT {
            self.clear_buffer_row(row - 1);

            for col in 0..BUFFER_WIDTH {
                let char = self.char_buffer.chars[row][col].read();

                self.char_buffer.chars[row - 1][col].write(char);
            }
        }

        self.clear_buffer_row(BUFFER_HEIGHT - 1);
    }

    fn clear_buffer_row(&mut self, row: usize) {
        for col in 0..BUFFER_WIDTH {
            self.char_buffer.chars[row][col].write(SingleChar {
                ascii_code: 0,
                colour_code: self.current_colour_code,
            })
        }
    }

    #[allow(dead_code)]
    pub fn reset_screen(&mut self) {
        for row in 0..BUFFER_HEIGHT {
            self.clear_buffer_row(row);
        }

        self.current_row_position = 0;
        self.current_column_position = 0;
    }

    fn update_cursor(&mut self) {
        self.insert_wrapper_space();

        let pos = (self.current_row_position * BUFFER_WIDTH) + self.current_column_position;
        self.write_to_cursor_location_register(pos);
    }

    fn insert_wrapper_space(&mut self) {
        self.pass_character_to_text_buffer(b' ');
    }

    fn write_to_cursor_location_register(&self, pos: usize) {
        use x86_64::instructions::port::Port;

        unsafe {
            let mut port_ctrl = Port::new(0x3d4);
            let mut port_data = Port::new(0x3d5);

            port_ctrl.write(0x0f as u8); //low byte
            port_data.write((pos & 0xff) as u8);
            port_ctrl.write(0x0e as u8); //high byte
            port_data.write(((pos >> 8) & 0xff) as u8);
        }
    }
}

impl fmt::Write for ScreenWriter {
    /* Derives write!() and writeln!() macros */
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.write_string(s);
        Ok(())
    }
}


#[test_case]
fn test_println_simple() {
    println!("test_println_simple output");
}

#[test_case]
fn test_println_many() {
    for _ in 0..70 {
        println!("test_println_many output");
    }
}

#[test_case]
fn test_println_output() {
    use x86_64::instructions::interrupts;

    let s = "This is some string";
    interrupts::without_interrupts(|| {
        WRITER.lock().reset_screen();
        println!("{}", s);

        for (i, character) in s.bytes().enumerate() {
            let screen_character = WRITER.lock().char_buffer.chars[0][i].read();

            assert_eq!(screen_character.ascii_code, character);
        }
    });
}

#[test_case]
fn test_println_multiline_output() {
    WRITER.lock().reset_screen();

    let s = "This is some string \n and this is written in another row";
    println!("{}", s);

    let mut row = 0;
    let mut col = 0;
    for character in s.bytes() {
        if character == b'\n' {
            row += 1;
            col = 0;
            continue;
        }

        let screen_character = WRITER.lock().char_buffer.chars[row][col].read();
        col += 1;

        assert_eq!(screen_character.ascii_code, character);
    }
}