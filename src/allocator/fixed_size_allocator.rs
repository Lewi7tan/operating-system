#![allow(unused_imports)]
#![allow(dead_code)]

use core::alloc::{GlobalAlloc, Layout};
use core::ptr;
use core::mem;
use core::ptr::NonNull;
use super::*;

const BLOCK_SIZES: &[usize] = &[8, 16, 32, 64, 128, 256, 512, 1024, 2048];

struct FreeBlock {
    next: Option<&'static mut FreeBlock>,
}

pub struct FixedSizeBlockAllocator {
    list_heads: [Option<&'static mut FreeBlock>; BLOCK_SIZES.len()],
    fallback_allocator: linked_list_allocator::Heap, //huge allocation size
}

impl FixedSizeBlockAllocator {
    pub const fn new() -> Self {
        const EMPTY: Option<&'static mut FreeBlock> = None;
        FixedSizeBlockAllocator {
            list_heads: [EMPTY; BLOCK_SIZES.len()],
            fallback_allocator: linked_list_allocator::Heap::empty(),
        }
    }

    pub unsafe fn init(&mut self, heap_start: usize, heap_size: usize) {
        self.fallback_allocator.init(heap_start, heap_size);
    }

    fn fallback_alloc(&mut self, layout: Layout) -> *mut u8 {
        match self.fallback_allocator.allocate_first_fit(layout) {
            Ok(ptr) => ptr.as_ptr(),
            Err(_) => ptr::null_mut(),
        }
    }
}

unsafe impl GlobalAlloc for Locked<FixedSizeBlockAllocator> {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        let mut allocator = self.lock();

        match size_index(&layout) {
            Some(index) => {
                match allocator.list_heads[index].take() {
                    Some(free_block) => {
                        allocator.list_heads[index] = free_block.next.take();

                        free_block as *mut FreeBlock as *mut u8
                    }
                    None => {
                        let block_size = BLOCK_SIZES[index];
                        let block_align = block_size;

                        let layout = Layout::from_size_align(block_size, block_align).unwrap();
                        allocator.fallback_alloc(layout)
                    }
                }
            }
            None => {
                allocator.fallback_alloc(layout)
            }
        }
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        let mut allocator = self.lock();

        match size_index(&layout) {
            Some(index) => {
                let new_free_block = FreeBlock {
                    next: allocator.list_heads[index].take(),
                };

                assert!(mem::size_of::<FreeBlock>() <= BLOCK_SIZES[index]);
                assert!(mem::align_of::<FreeBlock>() <= BLOCK_SIZES[index]);

                let new_free_block_ptr = ptr as *mut FreeBlock;
                new_free_block_ptr.write(new_free_block);
                allocator.list_heads[index] = Some(&mut *new_free_block_ptr);
            }
            None => {
                let ptr = NonNull::new(ptr).unwrap();
                allocator.fallback_allocator.deallocate(ptr, layout);
            }
        }
    }
}

/// Returns appropriate size for block with the given layout
fn size_index(layout: &Layout) -> Option<usize> {
    let required_block_size = layout.size().max(layout.align());
    BLOCK_SIZES.iter().position(|&s| s >= required_block_size)
}
