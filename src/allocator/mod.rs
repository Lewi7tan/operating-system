use alloc::alloc::{Layout};
use x86_64::{
    structures::paging::{
        mapper::MapToError, FrameAllocator, Mapper, Page, PageTableFlags, Size4KiB,
    },
    VirtAddr,
};

pub mod fixed_size_allocator;

use fixed_size_allocator::FixedSizeBlockAllocator;

/**************************************
 *********** ALLOCATOR ****************
 **************************************/

#[global_allocator]
static ALLOCATOR: Locked<FixedSizeBlockAllocator> = Locked::new(FixedSizeBlockAllocator::new());

#[alloc_error_handler]
fn alloc_error_handler(layout: Layout) -> ! {
    panic!("allocation error: {:?}", layout)
}

pub struct Locked<T> {
    inner: spin::Mutex<T>,
}

impl<T> Locked<T> {
    pub const fn new(inner: T) -> Self {
        Locked {
            inner: spin::Mutex::new(inner),
        }
    }

    pub fn lock(&self) -> spin::MutexGuard<T> {
        self.inner.lock()
    }
}

/**************************************
 *************** HEAP *****************
 **************************************/

pub const HEAP_START: usize = 0x_4444_4444_0000;
pub const HEAP_SIZE: usize = 100 * 1024; // 100 KiB


pub fn init_heap(mapper: &mut impl Mapper<Size4KiB>, frame_allocator: &mut impl FrameAllocator<Size4KiB>)
                 -> Result<(), MapToError<Size4KiB>> {
    let page_range = {
        let heap_start = VirtAddr::new(HEAP_START as u64);
        let heap_end = heap_start + HEAP_SIZE - 1u64;
        let heap_start_page = Page::containing_address(heap_start);
        let heap_end_page = Page::containing_address(heap_end);
        Page::range_inclusive(heap_start_page, heap_end_page)
    };

    for page in page_range {
        let frame = frame_allocator
            .allocate_frame()
            .ok_or(MapToError::FrameAllocationFailed)?;
        let flags = PageTableFlags::PRESENT | PageTableFlags::WRITABLE;

        unsafe {
            mapper.map_to(page, frame, flags, frame_allocator)?.flush();
        };
    }

    unsafe {
        ALLOCATOR.lock().init(HEAP_START, HEAP_SIZE);
    }

    Ok(())
}


/**************************************
 ************ TESTS *******************
 **************************************/

#[test_case]
fn test_box_allocation() {
    use alloc::boxed::Box;

    let heap1 = Box::new(127);
    let heap2 = Box::new(666);

    assert_eq!(*heap1, 127);
    assert_eq!(*heap2, 666);
}

#[test_case]
fn test_vec_allocation() {
    use alloc::vec::Vec;

    let n = 1000;
    let nth_partial_sum = 499500;

    let mut vec = Vec::new();
    for i in 0..n {
        vec.push(i);
    }

    assert_eq!(vec.iter().sum::<u64>(), nth_partial_sum);
}

#[test_case]
fn test_for_memory_reuse() {
    use alloc::boxed::Box;

    for i in 0..HEAP_SIZE {
        let x = Box::new(i);
        assert_eq!(*x, i);
    }
}

#[test_case]
fn test_for_allocation_behind_next_ptr() {
    use alloc::boxed::Box;
    use core::mem::drop;
    use crate::{serial_print, serial_println};

    let x: Box<u64> = Box::new(666);
    let x_location =&*x as *const u64 as usize;

    let y: Box<u64> = Box::new(777);
    let y_location = &*y as *const u64 as usize;

    let z: Box<u64> = Box::new(888);
    let z_location = &*z as *const u64 as usize;

    drop(y);

    let new_var: Box<u8> = Box::new(100);
    let new_var_location = &*new_var as *const u8 as usize;


    assert!(x_location < new_var_location);
    assert!(new_var_location < z_location);
    assert_eq!(y_location, new_var_location);
}