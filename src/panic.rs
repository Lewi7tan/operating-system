use core::panic::PanicInfo;

#[allow(unused_imports)]
use crate::{print, println, serial_print, serial_println};

#[cfg(test)]
use crate::tests::{exit_qemu, QemuExitCode};

#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! { //&PanicInfo contains file and line where panic was called and a panic message
    println!("{}", info);
    loop {
        x86_64::instructions::hlt();
    }
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    serial_println!("[failed]\n");
    serial_println!("Error: {}\n", info);

    exit_qemu(QemuExitCode::Fail);
    loop {
        x86_64::instructions::hlt();
    }
}