#![no_main]
#![no_std]
#![feature(custom_test_frameworks)]
#![feature(alloc_error_handler)]
#![feature(const_mut_refs)]
#![feature(int_log)]
#![test_runner(test_runner)]
#![reexport_test_harness_main = "test_main"]
#![feature(asm)]
#![feature(abi_x86_interrupt)]

extern crate alloc;

mod panic;
mod drivers;
mod tests;
mod interrupts;
mod gdt;
mod memory;
mod allocator;

#[cfg(test)]
use crate::tests::test_runner;
use bootloader::BootInfo;


#[no_mangle]
pub extern "C" fn _start(boot_info: &'static BootInfo) -> ! {
    interrupts::init_idt();
    gdt::init();
    unsafe { interrupts::PICS.lock().initialize() };
    x86_64::instructions::interrupts::enable();

    println!("Hello friend . . .");

    use x86_64::VirtAddr;
    let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);
    let mut mapper = unsafe { memory::init(phys_mem_offset) };
    let mut frame_allocator = unsafe {
        memory::BootInfoFrameAllocator::init(&boot_info.memory_map)
    };

    allocator::init_heap(&mut mapper, &mut frame_allocator).expect("heap initialization failed");


    use alloc::boxed::Box;
    let x = Box::new(41);
    println!("{:p}: {}", x, x);

    use alloc::string::String;
    let message = String::from("Dupa pawiana");
    println!("{}", message);


    #[cfg(test)]
        test_main();

    loop {
        x86_64::instructions::hlt();
    }
}
